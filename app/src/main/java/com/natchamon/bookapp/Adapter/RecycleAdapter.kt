package com.natchamon.bookapp.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.natchamon.bookapp.*
import com.natchamon.bookapp.Data.DataMenu

class RecycleAdapter(private val dataList: Context)
    : RecyclerView.Adapter<RecycleAdapter.DataViewHolder>(){

    var menuList = emptyList<DataMenu>()
    internal fun setDataList(menuList: List<DataMenu>) {
        this.menuList = menuList
        notifyDataSetChanged()
    }

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
         val image : ImageView = itemView.findViewById(R.id.image)

        val txtdetail : TextView = itemView.findViewById(R.id.txtdetail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_item_homepage, parent, false)
        return DataViewHolder(view)
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        var data = menuList[position]


        holder.image.setImageResource(data.image)
        holder.txtdetail.text = data.txtdetail
        holder.image.setOnClickListener{
                if (position == 0) {
                    val context = holder.image.context
                    val intent = Intent(context, MenuActivity::class.java)
                    context.startActivity(intent)
                }
            if (position == 1) {
                val context = holder.image.context
                val intent = Intent(context, MenuActivity1::class.java)
                context.startActivity(intent)
            }
            if (position == 2) {
                val context = holder.image.context
                val intent = Intent(context, MenuActivity2::class.java)
                context.startActivity(intent)
            }
            if (position == 3) {
                val context = holder.image.context
                val intent = Intent(context, MenuActivity3::class.java)
                context.startActivity(intent)
            }
            if (position == 4) {
                val context = holder.image.context
                val intent = Intent(context, MenuActivity4::class.java)
                context.startActivity(intent)
            }


        }
    }

    override fun getItemCount() = menuList.size


}

