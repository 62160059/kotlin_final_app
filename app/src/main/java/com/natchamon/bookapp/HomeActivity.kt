package com.natchamon.bookapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.natchamon.bookapp.Adapter.RecycleAdapter
import com.natchamon.bookapp.Data.DataMenu

class HomeActivity : AppCompatActivity() {
    private  lateinit var recyclerView: RecyclerView
    private lateinit var  dataAdapter: RecycleAdapter
    private  var datalist = mutableListOf<DataMenu>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = GridLayoutManager(applicationContext,1)
        dataAdapter = RecycleAdapter(applicationContext)
        recyclerView.adapter = dataAdapter


        datalist.add(DataMenu( R.drawable.airi , "Airi"))
        datalist.add(DataMenu( R.drawable.violet, "Violet"))
        datalist.add(DataMenu( R.drawable.florentino,"Florentino"))
        datalist.add(DataMenu( R.drawable.thane,"Thane"))
        datalist.add(DataMenu( R.drawable.wukong,"Wukong"))


        dataAdapter.setDataList(datalist)

    }


}