package com.natchamon.bookapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MenuActivity2 : AppCompatActivity() {
    var btnNext : Button?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu3)
        btnNext = findViewById<Button>(R.id.btnNext)
        btnNext!!.setOnClickListener {
            var intent = Intent(this, MenuActivity2_2::class.java)
            startActivity(intent)
        }
    }
}